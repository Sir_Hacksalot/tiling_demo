﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace TilingDemo.Input
{
    class AllInputButtons
    {
        private const Keys kA_ButtonKey = Keys.K;
        private const Keys kB_ButtonKey = Keys.L;
        private const Keys kX_ButtonKey = Keys.J;
        private const Keys kY_ButtonKey = Keys.I;
        private const Keys kBack_ButtonKey = Keys.F1;
        private const Keys kStart_ButtonKey = Keys.F2;
        private const Keys kLB_ButtonKey = Keys.U;
        private const Keys kRB_ButtonKey = Keys.O;

        private ButtonState GetState(ButtonState gamePadButtonState, Keys key)
        {
            if (Keyboard.GetState().IsKeyDown(key))
            {
                return ButtonState.Pressed;
            }

            if (GamePad.GetState(PlayerIndex.One).IsConnected)
            {
                return gamePadButtonState;
            }

            return ButtonState.Released;
        }

        public ButtonState A { get { return GetState(GamePad.GetState(PlayerIndex.One).Buttons.A, 
            kA_ButtonKey); } }
        public ButtonState B { get { return GetState(GamePad.GetState(PlayerIndex.One).Buttons.B, 
            kB_ButtonKey); } }
        public ButtonState X { get { return GetState(GamePad.GetState(PlayerIndex.One).Buttons.X, 
            kX_ButtonKey); } }
        public ButtonState Y { get { return GetState(GamePad.GetState(PlayerIndex.One).Buttons.Y, 
            kY_ButtonKey); } }
        public ButtonState Start { get { return GetState(GamePad.GetState(PlayerIndex.One).Buttons.Start, 
            kStart_ButtonKey); } }
        public ButtonState Back { get { return GetState(GamePad.GetState(PlayerIndex.One).Buttons.Back, 
            kBack_ButtonKey); } }
        public ButtonState RB { get { return GetState(GamePad.GetState(PlayerIndex.One).Buttons.RightShoulder, 
            kRB_ButtonKey); } }
        public ButtonState LB { get { return GetState(GamePad.GetState(PlayerIndex.One).Buttons.LeftShoulder, 
            kLB_ButtonKey); } }



    }
}
