﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace TilingDemo.Input
{
    class AllInputTriggers
    {
        private const Keys kLeftTrigger = Keys.N;
        private const Keys kRightTrigger = Keys.M;
        const float kKeyTriggerValue = 0.75f;

        private float GetTriggerState(float gamePadTrigger, Keys key)
        {
            if (Keyboard.GetState().IsKeyDown(key))
            {
                return kKeyTriggerValue;
            }

            if (GamePad.GetState(PlayerIndex.One).IsConnected)
            {
                return gamePadTrigger;
            }

            return 0f;
        }

        public float Left
        {
            get
            {
                return GetTriggerState(
                    GamePad.GetState(PlayerIndex.One).Triggers.Left,
                    kLeftTrigger);
            }
        }

        public float Right
        {
            get
            {
                return GetTriggerState(
                    GamePad.GetState(PlayerIndex.One).Triggers.Right,
                    kRightTrigger);
            }
        }

    }
}
