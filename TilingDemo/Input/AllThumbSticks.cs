﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace TilingDemo.Input
{
    class AllThumbSticks
    {
        const Keys KLeftThumbStickUp = Keys.W;
        const Keys KLeftThumbStickDown = Keys.S;
        const Keys KLeftThumbStickLeft = Keys.A;
        const Keys KLeftThumbStickRight = Keys.D;

        const Keys KRightThumbStickUp = Keys.Up;
        const Keys KRightThumbStickDown = Keys.Down;
        const Keys KRightThumbStickLeft = Keys.Left;
        const Keys KRightThumbStickRight = Keys.Right;

        const float kKeyDownValue = 0.75f;

        private Vector2 ThumbStickState(Vector2 thumbStickValue,
            Keys up, Keys down, Keys left, Keys right)
        {
            Vector2 vec = Vector2.Zero;

            if (GamePad.GetState(PlayerIndex.One).IsConnected)
            {
                vec = thumbStickValue;
            }

            if (Keyboard.GetState().IsKeyDown(up))
            {
                vec.Y += kKeyDownValue;
            }

            if (Keyboard.GetState().IsKeyDown(down))
            {
                vec.Y -= kKeyDownValue;
            }

            if (Keyboard.GetState().IsKeyDown(left))
            {
                vec.X -= kKeyDownValue;
            }

            if (Keyboard.GetState().IsKeyDown(right))
            {
                vec.X += kKeyDownValue;
            }

            return vec;
        }

        public Vector2 Left
        {
            get
            {
                return ThumbStickState(
                    GamePad.GetState(PlayerIndex.One).ThumbSticks.Left,
                    KLeftThumbStickUp,
                    KLeftThumbStickDown,
                    KLeftThumbStickLeft,
                    KLeftThumbStickRight);
            }
        }

        public Vector2 Right
        {
            get
            {
                return ThumbStickState(
                    GamePad.GetState(PlayerIndex.One).ThumbSticks.Right,
                    KRightThumbStickUp,
                    KRightThumbStickDown,
                    KRightThumbStickLeft,
                    KRightThumbStickRight);
            }
        }
    }
}
