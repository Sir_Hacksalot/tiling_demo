﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended;
using MonoGame.Extended.BitmapFonts;
using MonoGame.Extended.Graphics;
using MonoGame.Extended.Sprites;
using MonoGame.Extended.Tiled;
using MonoGame.Extended.ViewportAdapters;
using System;
using System.Collections.Generic;
//using TilingDemo.InputClasses;
using TilingDemo.Input;

namespace TilingDemo
{
    public class Game1 : Game
    {
        static public GraphicsDeviceManager graphicsDeviceManager;
        static public SpriteBatch spriteBatch;
        static public ContentManager contentManager;
        private Sprite sprite;
        private Camera2D camera;
        private TiledMapRenderer mapRenderer;
        private ViewportAdapter viewportAdapter;
        private BitmapFont bitmapFont;
        private TiledMap map;
        private Effect customEffect;
        private Queue<string> availableMaps;
        private KeyboardState keyboardState;
        private KeyboardState prevKeyboardState;


        public Game1()
        {
            graphicsDeviceManager = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            Window.AllowUserResizing = true;
        }

        
        protected override void Initialize()
        {
            viewportAdapter = new BoxingViewportAdapter(Window, GraphicsDevice, 800, 480);
            camera = new Camera2D(viewportAdapter);
            camera.MaximumZoom = 2.0f;
            camera.MinimumZoom = 1.0f;

            mapRenderer = new TiledMapRenderer(GraphicsDevice);

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            bitmapFont = Content.Load<BitmapFont>(@"BitmapFonts\rectecfont");

            availableMaps = new Queue<string>(new[] { "level01"});
            map = LoadNextMap();

            camera.LookAt(new Vector2(map.WidthInPixels, map.HeightInPixels) * 0.5f);

            var effect = new CustomEffect(GraphicsDevice)
            {
                Alpha = 0.0f,
                TextureEnabled = true,
                VertexColorEnabled = false
            };
            customEffect = effect;

        }

        
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }


        private TiledMap LoadNextMap()
        {
            var name = availableMaps.Dequeue();
            map = Content.Load<TiledMap>($"TileMaps/{name}");
            availableMaps.Enqueue(name);
            return map;
        }

        protected override void Update(GameTime gameTime)
        {
            var deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            Vector2 moveDirection = Vector2.Zero;

            const float cameraSpeed = 150f;
            const float zoomSpeed = 1f;

            if (InputWrapper.Buttons.Back == ButtonState.Pressed)
            {
                this.Exit();
            }

            if (InputWrapper.Triggers.Right > 0)
            {
                float triggerAmount = InputWrapper.Triggers.Right;
                camera.ZoomIn(zoomSpeed * deltaTime * triggerAmount);
            }

            if (InputWrapper.Triggers.Left > 0)
            {
                float triggerAmount = InputWrapper.Triggers.Left;
                camera.ZoomOut(zoomSpeed * deltaTime * triggerAmount);
            }



            moveDirection += InputWrapper.ThumbSticks.Left;

            camera.Move(MoveCamera(moveDirection, cameraSpeed, deltaTime));

            base.Update(gameTime);
        }

        private Vector2 MoveCamera(Vector2 moveDirection, float cameraSpeed, 
            Single deltaTime)
        {
            Vector2 cameraVector = Vector2.Zero;

            var isCameraMoving = moveDirection != Vector2.Zero;

            if(isCameraMoving)
            {
                moveDirection.Normalize();
                cameraVector = moveDirection * cameraSpeed * deltaTime;
            }

            return cameraVector;
        }

        private void LookAtMapCentre()
        {
            switch (map.Orientation)
            {
                case TiledMapOrientation.Orthogonal:
                    camera.LookAt(new Vector2(map.WidthInPixels, map.HeightInPixels) * 0.5f);
                    break;
                case TiledMapOrientation.Isometric:
                    camera.LookAt(new Vector2(0, map.HeightInPixels + map.TileHeight) * 0.5f);
                    break;
                case TiledMapOrientation.Staggered:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            GraphicsDevice.BlendState = BlendState.AlphaBlend;
            GraphicsDevice.SamplerStates[0] = SamplerState.PointClamp;
            GraphicsDevice.RasterizerState = RasterizerState.CullNone;

            Matrix viewMatrix = camera.GetViewMatrix();
            Matrix projectionMatrix = Matrix.CreateOrthographicOffCenter(0, GraphicsDevice.Viewport.Width,
                GraphicsDevice.Viewport.Height, 0, 0f, -1f);

            mapRenderer.Draw(map, viewMatrix, projectionMatrix, customEffect);

            //spriteBatch.Begin(blendState: BlendState.AlphaBlend, transformMatrix: camera.GetViewMatrix());
            //spriteBatch.Draw(sprite);
            //spriteBatch.End();

            spriteBatch.Begin();

            spriteBatch.DrawString(bitmapFont, 
                "I am the hero of time", 
                new Vector2(10, 10), 
                Color.White);

            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
