﻿using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Graphics.Effects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TilingDemo
{
    class CustomEffect : DefaultEffect, ITiledMapEffect
    {
        public CustomEffect(GraphicsDevice graphicsDevice) : base(graphicsDevice)
        {
            //nothing doing....
        }

        public CustomEffect(GraphicsDevice graphicsDevice, byte[] byteCode) : base(graphicsDevice, byteCode)
        {
            //nothing doing....
        }

        public CustomEffect(Effect cloneSource) : base(cloneSource)
        {
            //nothing doing....
        }
    }
}
