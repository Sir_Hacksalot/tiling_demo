﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended;
using MonoGame.Extended.ViewportAdapters;

namespace TilingDemo
{
    class Camera : Camera2D
    {
        private Rectangle? limits;
        private ViewportAdapter viewportAdapter;
        private Vector2 position;
        private float zoom;

        public Camera(ViewportAdapter viewportAdapter) : base(viewportAdapter)
        {
            this.viewportAdapter = viewportAdapter;
        }

        private void ValidateZoom()
        {
            if(limits.HasValue)
            {
                float MinimumZoomX = (float)viewportAdapter.VirtualWidth / limits.Value.Width;
                float MinimumZoomY = (float)viewportAdapter.VirtualHeight / limits.Value.Height;
                zoom = MathHelper.Max(zoom, MathHelper.Max(MinimumZoomX, MinimumZoomY));
            }
        }
        private void ValidatePosition()
        {
            if (limits.HasValue)
            {
                Vector2 cameraWorldMin = Vector2.Transform(Vector2.Zero, GetInverseViewMatrix());
                Vector2 cameraSize = new Vector2(viewportAdapter.VirtualWidth, viewportAdapter.VirtualHeight) / zoom;
                Vector2 limitWorldMin = new Vector2(limits.Value.Left, limits.Value.Top);
                Vector2 limitWorldMax = new Vector2(limits.Value.Right, limits.Value.Bottom);
                Vector2 positionOffset = position - cameraWorldMin;
                position = Vector2.Clamp(cameraWorldMin, limitWorldMin, limitWorldMax - cameraSize) + positionOffset;
            }
        }

        public new Vector2 Position
        {
            get { return position; }
            set
            {
                position = value;
                ValidatePosition();
            }
        }

        public new float Zoom
        {
            get { return zoom; }
            set
            {
                zoom = value;
                ValidateZoom();
                ValidatePosition();
            }
        }

        public Rectangle? Limits
        {
            set
            {
                limits = value;
                ValidateZoom();
                ValidatePosition();
            }
        }


    }
}
