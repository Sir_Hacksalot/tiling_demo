﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace TilingDemo
{
    class GameObject : TexturedPrimitive
    {
        public GameObject(string imageName, Vector2 position, Vector2 size) : base(imageName, position, size)
        {
            //
        }
    }
}
