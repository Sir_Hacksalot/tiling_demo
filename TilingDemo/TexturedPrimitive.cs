﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace TilingDemo
{
    public class TexturedPrimitive
    {
        private Texture2D image;
        private Vector2 position;
        private Vector2 size;

        public Texture2D Image
        {
            get {return image; }
            set { image = value; }
        }
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }
        public Vector2 Size
        {
            get { return size; }
            set { size = value; }
        }

        public TexturedPrimitive(String imageName, Vector2 position, Vector2 size)
        {
            Image = Game1.contentManager.Load<Texture2D>(imageName);
            Position = position;
            Size = size;
        }

        public void Update(Vector2 deltaTranslate, Vector2 deltaScale)
        {
            Position += deltaTranslate;
            Size += deltaScale;
        }

        public void Draw()
        {
            //defines where and size of the texture to show
            Rectangle destRect = new Rectangle((int)Position.X, (int)Position.Y,
                                                (int)Size.X, (int)Size.Y);
            Game1.spriteBatch.Draw(Image, destRect, Color.White);
        }
  
    }
}
