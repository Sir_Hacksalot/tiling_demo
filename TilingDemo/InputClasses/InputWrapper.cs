﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TilingDemo.InputClasses
{
    class InputWrapper
    {
        static public AllInputButtons Buttons = new AllInputButtons();
        static public AllThumbSticks ThumbSticks = new AllThumbSticks();
        static public AllInputTriggers Triggers = new AllInputTriggers();
    }
}
