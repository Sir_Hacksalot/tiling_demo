﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace TilingDemo.InputClasses
{
    class AllThumbSticks
    {
        const Keys kLeftThumbStickUp = Keys.W;
        const Keys kLeftThumbStickDown = Keys.S;
        const Keys kLeftThumbStickLeft = Keys.A;
        const Keys kLeftThumbStickRight = Keys.D;

        const Keys kRightThumbStickUp = Keys.Up;
        const Keys kRightThumbStickDown = Keys.Down;
        const Keys kRightThumbStickLeft = Keys.Left;
        const Keys kRightThumbStickRight = Keys.Right;

        const float kKeyDownValue = 0.075f;

        private Vector2 ThumbStickState(Vector2 thumbStickValue, 
            Keys up, Keys down, Keys left, Keys right)
        {
            Vector2 moveDirection = Vector2.Zero;

            if(GamePad.GetState(PlayerIndex.One).IsConnected)
            {
                moveDirection = thumbStickValue;
            }

            if(Keyboard.GetState().IsKeyDown(up))
            {
                moveDirection.Y += kKeyDownValue;
            }

            if (Keyboard.GetState().IsKeyDown(down))
            {
                moveDirection.Y -= kKeyDownValue;
            }

            if (Keyboard.GetState().IsKeyDown(left))
            {
                moveDirection.X -= kKeyDownValue;
            }

            if (Keyboard.GetState().IsKeyDown(right))
            {
                moveDirection.X += kKeyDownValue;
            }

            return moveDirection;
        }

        public Vector2 Left
        {
            get
            {
                return ThumbStickState(GamePad.GetState(PlayerIndex.One).ThumbSticks.Left,
                    kLeftThumbStickUp, kLeftThumbStickDown,
                    kLeftThumbStickLeft, kLeftThumbStickRight);
            }
        }
        public Vector2 Right
        {
            get
            {
                return ThumbStickState(GamePad.GetState(PlayerIndex.One).ThumbSticks.Right,
                    kRightThumbStickUp, kRightThumbStickDown,
                    kRightThumbStickLeft, kRightThumbStickRight);
            }
        }
    }
}
